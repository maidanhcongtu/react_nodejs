var express = require('express');
var router = express.Router();

/*admin web*/
	router.get('/admin/questions/', function(req, res, next) {
		res.render('index', { title: 'Quản lí câu hỏi' });
	});

	router.get('/admin/exams/', function(req, res, next) {
		res.render('index', { title: 'Quản lí bài thi' });
	});

	router.get('/admin/exam-parts/', function(req, res, next) {
		res.render('index', { title: 'Quản lí phần thi' });
	});

/*user web*/
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/user', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/product/:keyname', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;
