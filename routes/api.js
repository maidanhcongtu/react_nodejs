const express = require('express');
const router = express.Router();
const multer = require("multer");
const path = require('path');
const mkdirp = require('mkdirp');

const Service = require('../config/service.js');
const StringUtil = require("../utils/string-util.js");
const NumberUtil = require("../utils/number-util.js");
const MiddleWareValidate = require("../middleware/validate.js");


/* === INITIALIZE DATA === */
	const storageImage = multer.diskStorage({
	    destination: function (req, file, cb) {
	    	const dir = './public/uploads/froala/images/';
	    	mkdirp(dir, err => cb(err, dir));
	    },
	    filename: function (req, file, cb) {
	        cb(null, StringUtil.toCurrentStrDate() + path.extname(file.originalname));
	  	}
	});

	const storageFile = multer.diskStorage({
	    destination: function (req, file, cb) {
	    	const dir = './public/uploads/froala/files/';
	    	mkdirp(dir, err => cb(err, dir));
	    },
	    filename: function (req, file, cb) {
	        cb(null, StringUtil.toCurrentStrDate() + path.extname(file.originalname));
	  	}
	});

	const storageVideo = multer.diskStorage({
	    destination: function (req, file, cb) {
	    	const dir = './public/uploads/froala/videos/';
	    	mkdirp(dir, err => cb(err, dir));
	    },
	    filename: function (req, file, cb) {
	        cb(null, StringUtil.toCurrentStrDate() + path.extname(file.originalname));
	  	}
	});

	const uploadImage = multer({ storage: storageImage });
	const uploadFile = multer({ storage: storageFile });
	const uploadVideo = multer({ storage: storageVideo });

/* ==== FROALA API ==== */
	/*SAVE*/
	router.post('/api/froala/images', uploadImage.single("file"), function(req, res) {
		var filePath = StringUtil.formatPath("\\" + req.file.path);
		res.json({
			link: filePath
		});
	});

	router.post('/api/froala/videos', uploadVideo.single("file"), function(req, res, next) {
		var filePath = StringUtil.formatPath("\\" + req.file.path);
		res.json({
			link: filePath
		});
	});

	router.post('/api/froala/files', uploadFile.single("file"), function(req, res) {
		var filePath = StringUtil.formatPath("\\" + req.file.path);
		res.json({
			link: filePath
		});
	});

/* ==== QUESTION API ==== */
	/*SAVE*/
	router.post('/api/admin/questions/', MiddleWareValidate.Question,  function(req, res, next) {
		//update tag
		Service.TagEntity.update("question", req.body.tags, function(){});
		//save question
		Service.QuestionEntity.save(req.body, function(err, product) {
			res.json(product);
		});

	});

	/*UPDATE*/
	router.put('/api/admin/questions/', MiddleWareValidate.Question, function(req, res, next) {
		//update tag
		Service.TagEntity.update("question", req.body.tags, function(err, product){});
		//save question
		Service.QuestionEntity.update(req.body, function(err, product) {
			res.json(product);
		});
	});

	/* GET ALL */
	router.get('/api/admin/questions/', function(req, res, next) {
		Service.QuestionEntity.getAll(function(err, product) {
			res.json(product);
		});
	});

	/* FILTER QUESTION */
	router.get('/api/admin/questions/filter', function(req, res, next) {
		var tags = req.query.tags;
		var type = req.query.type;
		var fromDate = req.query.fromDate;
		var toDate = req.query.toDate;
		Service.QuestionEntity.findByTagsAndTypeAndFromDateToDate(tags, type, fromDate, toDate, function(err, products) {
			res.json(products);
		});
	});

	/* DELETE BY ID */
	router.delete('/api/admin/questions/:id', function(req, res, next) {
		Service.QuestionEntity.findByIdAndRemove(req.params.id, function(err, product) {
			res.json(product);
		});
	});

/* ==== EXAM PART API ==== */
	/*SAVE*/
	router.post('/api/admin/exam-parts/', MiddleWareValidate.ExamPart,  function(req, res, next) {
		//update tag
		Service.TagEntity.update("exam-part", req.body.tags, function(){});

		//save exam part
		Service.ExamPartEntity.save(req.body, function(err, product) {
			res.json(product);
		});
	});

	router.get('/api/admin/exam-parts/', function(req, res, next) {
		Service.ExamPartEntity.getAll(function(err, product) {
			res.json(product);
		});
	});

	/* DELETE BY ID */
	router.delete('/api/admin/exam-parts/:id', function(req, res, next) {
		Service.ExamPartEntity.findByIdAndRemove(req.params.id, function(err, product) {
			res.json(product);
		});
	});

	/*UPDATE*/
	router.put('/api/admin/exam-parts/', MiddleWareValidate.ExamPart, function(req, res, next) {
		//update tag
		Service.TagEntity.update("exam-part", req.body.tags, function(){});
		//update exam part
		Service.ExamPartEntity.update(req.body, function(err, product) {
			res.json(product);
		});
	});

/* ==== TAG API ==== */
	router.get('/api/admin/tags/:type', function(req, res, next) {
		Service.TagEntity.getByType(req.params.type, function(err, product) {
			res.json(product);
		});
	});

module.exports = router;
