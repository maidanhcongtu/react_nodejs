var path = require('path');
var extend = require('extend');
var db = require(path.resolve(__dirname, "./db.js"));
var EntityConvertor = require("../convertor/entity-convertor.js");

var Service = {
		QuestionEntity: {

			save : function(questionVO, callback) {
				var Question = new db.Question(EntityConvertor.Question.toEntity(questionVO));
				Question.save(function(error, product) {
					db.Question.findOne({_id: product._id})
						.populate("childrenQuestions")
						.exec(function(err, product) { 
							callback(err, product);
						});
				});
			},

			update : function(questionVO, callback) {
				var questionEntity = EntityConvertor.Question.toEntity(questionVO);
				db.Question.update({_id: questionVO._id}, questionEntity, function() {
					db.Question.findOne({_id: questionVO._id})
						.populate("childrenQuestions")
						.exec(function(err, product) { 
							callback(err, product);
						});
				});
			},

			getAll: function(callback) {
				db.Question.find({})
					.populate("childrenQuestions")
					.exec(function(err, product) { 
						callback(err, product);
					});
			},

			findByIdAndRemove: function(questionId, callback) {
				db.Question.findByIdAndRemove(questionId, function(err, product) { 
					callback(err, product);
				});
			},

			findByTagsAndTypeAndFromDateToDate: function(tags, type, fromDate, toDate, callback) {
				var filter = {};
				
				if(type != "ALL") {
					extend(filter, {type: type});
				}

				if(tags && tags.length > 0) {
					extend(filter, {tags: { "$in" : tags}});
				}

				extend(filter, {updatedAt: {"$gte": fromDate, "$lte": toDate}});

				db.Question.find(filter, function(err, docs){
					callback(err, docs);
				})
			}

		},
		ExamPartEntity: {

			save : function(examPartVO, callback) {
				var ExamPart = new db.ExamPart(EntityConvertor.ExamPart.toEntity(examPartVO));
				ExamPart.save(function(error, product) {
					db.ExamPart.findOne({_id: product._id})
						.populate("questionScores.question")
						.exec(function(err, product) { 
							callback(err, product);
						});
				});
			},

			update : function(examPartVO, callback) {
				var examPartEntity = EntityConvertor.ExamPart.toEntity(examPartVO);
				db.ExamPart.update({_id: examPartVO._id}, examPartEntity, function() {
					db.ExamPart.findOne({_id: examPartVO._id})
						.populate("questionScores.question")
						.exec(function(error, product){
							callback(error, product);
					});
				});
			},

			getAll: function(callback) {
				db.ExamPart.find({})
					.populate("questionScores.question")
					.exec(function(err, product) { 
						callback(err, product);
					});
			},

			findByIdAndRemove: function(partId, callback) {
				db.ExamPart.findByIdAndRemove(partId, function(err, product) { 
					callback(err, product);
				});
			},

			findByTagsAndTypeAndFromDateToDate: function(tags, type, fromDate, toDate, callback) {
				var filter = {};
				
				if(type != "ALL") {
					extend(filter, {type: type});
				}

				if(tags && tags.length > 0) {
					extend(filter, {tags: { "$in" : tags}});
				}

				extend(filter, {updatedAt: {"$gte": fromDate, "$lte": toDate}});

				db.Question.find(filter, function(err, docs){
					callback(err, docs);
				})
			}

		},
		TagEntity: {
			update : function(type, tags, callback) {
				this.getByType(type, function(error, product){
					//save tags with type
					if(product == null) {	
						var Tag = new db.Tag({type: type, tags: tags});
						Tag.save(function(error, product) {
							console.log(error);
						});
					} else {
						//update
						var buildNewTags = function(newTags, existingTags) {
							for(var i in newTags) {
								var duplicateTags = existingTags.filter((val, idx) => {
									return newTags[i].toLocaleLowerCase() == val.toLocaleLowerCase();
								});

								if(duplicateTags.length == 0) {
									existingTags.push(newTags[i]);
								} 
							};
							return existingTags;
						}
						
						product.tags = buildNewTags(tags, product.tags);
						//update tags
						db.Tag.update({_id: product._id}, product, function(error, numAffected){
						});
					}
				});
			},

			getAll: function(callback) {
				db.Tag.find({}, function(err, product) {
					callback(err, product);
				});
			},

			getByType: function(type, callback) {
				db.Tag.findOne({type: type}, function(err, product) { 
					callback(err, product);
				});
			},
		}
}

module.exports = Service;

