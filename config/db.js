var mongoose = require('mongoose');
var ModifySchemaData = require("../middleware/modify-schema.js")
mongoose.connect('mongodb://localhost/reactnode');
//mongoose.connect('mongodb://admin:QTKwYlBrHSsG@127.11.161.2:27017/reactnode');
var Schema = mongoose.Schema;

/* == DEFINE SCHEMA == */
	/*TAGS*/
		var tagSchema = new Schema({
				type : String, 
				tags: [String], 
				createdAt: Number, 
				updatedAt: Number
		});
		ModifySchemaData.Pre(tagSchema);
		var Tag = mongoose.model('tag', tagSchema);


	/*QUESTION*/
		var questionSchema = new Schema({
				content: String, 
				hint: String, 
				type : String, 
				tags: [String], 
				answers: Array, 
				childrenQuestions: [{ type: Schema.Types.ObjectId, ref: 'questions' }], 
				createdAt: Number, 
				updatedAt: Number
		});
		ModifySchemaData.Pre(questionSchema);
		var Question = mongoose.model('questions', questionSchema);

	/*EXAM PART*/
		var examPartSchema = new Schema({
				tags: [String], 
				title: String, 
				description: String, 
				type : String, 
				questionScores: [{
					score: Number,
					question: { type: Schema.Types.ObjectId, ref: 'questions' }
				}], 
				createdAt: Number, 
				updatedAt: Number
		});
		ModifySchemaData.Pre(examPartSchema);
		var ExamPart = mongoose.model('examparts', examPartSchema);

module.exports = {
	Question : Question,
	ExamPart : ExamPart,
	Tag : Tag
}