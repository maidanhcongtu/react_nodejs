const StringUtil = require("../utils/string-util.js");

var EntityConvertor = function() {
	return {
		Question: {
			toEntity: function(questionVO) {
				var answers = [];
				//build answer question when type is one choice or multi choice
				if(questionVO.type == "ONE_CHOICE" || questionVO.type == "MULTIPLE_CHOICE") {
					for(var i in questionVO.answers) {
						var answer = questionVO.answers[i];
						if(!StringUtil.isEmpty(answer.content)) {
							answers.push({
								content : answer.content,
								order : i,				
								correctAnswer : answer.correctAnswer
							});
						}
					}
				}

				var childrenQuestions = [];
				// build list question when type is group question
				if(questionVO.type == "GROUP") {
					childrenQuestions = questionVO.childrenQuestions.map(function(val, idx){
						return val._id;
					});
				}

				return {
					content : questionVO.content,
					hint : questionVO.hint,
					type : questionVO.type,
					tags : questionVO.tags,
					answers : answers,
					childrenQuestions: childrenQuestions
				}
			}
		},
		ExamPart: {
			toEntity: function(examPartVO) {
				// build list question score
				var questionScores = examPartVO.questionScores.map(function(val, idx){
					return {score: val.score, question: val.question._id};
				});
				
				return {
					tags : examPartVO.tags,
					title : examPartVO.title,
					description : examPartVO.description,
					questionScores : questionScores
				}
			}
		}
	}
}

module.exports = new EntityConvertor();