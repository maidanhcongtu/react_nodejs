var ModifySchemaData = function() {
	return {
		Pre: function(schema) {
			schema.pre("save", function(next) {
				this.createdAt = new Date().getTime();
				this.updatedAt = new Date().getTime();
				next();
			});
		}
	}
};

module.exports = new ModifySchemaData();

