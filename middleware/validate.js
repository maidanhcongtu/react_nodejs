var MiddleWareValidation = function() {
	return {
		Question: function(req, res, next) {
			var question = req.body;
			var e = new Error();
			e.status = 400;
			if(!question.type || question.type.trim().length == 0) {
				e.message = "Question type can not be empty";
				next(e);
				return;
			} 
			if(!question.tags || question.tags.length == 0) {
				e.message = "Question tags can not be empty";
				next(e);
				return;
			} 
			
			next();
		}, 
		ExamPart: function(req, res, next) {
			var examPart = req.body;
			var e = new Error();
			e.status = 400;
			
			if(!examPart.tags || examPart.tags.length == 0) {
				e.message = "Exam part tags can not be empty";
				next(e);
				return;
			}

			if(!examPart.title || examPart.title.trim().length == 0) {
				e.message = "Title of exam part can not be empty";
				next(e);
				return;
			}

			if(!examPart.questionScores || examPart.questionScores.length == 0) {
				e.message = "Exam part doesnt have any question";
				next(e);
				return;
			}

			next();
		}
	}
}

module.exports = new MiddleWareValidation();