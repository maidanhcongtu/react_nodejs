var StringUtil = function() {
	return {
		toCurrentStrDate: function() {
			const formatTwoNumber = function(number) {
				if(parseInt(number) < 10) {
					return "0" + parseInt(number);
				}
				return parseInt(number);
			}
			var d = new Date();
			return d.getFullYear() + "-" + formatTwoNumber(d.getMonth() + 1) + "-" + formatTwoNumber(d.getDate()) + "-" + formatTwoNumber(d.getHours()) + "-" + formatTwoNumber(d.getMinutes()) + "-" + formatTwoNumber(d.getSeconds()) + "-" + formatTwoNumber(d.getMilliseconds());
		},
		formatPath: function(strPath) {
			return strPath.replace(/(\\|\\\\)/g, "/");
		},
		isEmpty: function(str) {
			if(str == null || str.trim() != "")
				return true;
			return false;
		}

	}
}

module.exports = new StringUtil();