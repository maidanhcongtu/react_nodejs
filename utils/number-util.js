var NumberUtil = function() {
	return {
		buildFromDayByYearMonthDay: function(year, month, day) {
			var currentDate = new Date();
			year = year && (year >= 0 && year <= 9999) ? year : currentDate.getFullYear();
			month = month && (month >= 1 && month <= 11) ? (month - 1) : 0;
			day = day && (day >= 1 && day <= 31) ? day : 1;
			return new Date(year, month, day, 0, 0, 0, 0).getTime();
		},
		buildToDayByYearMonthDay: function(year, month, day) {
			var currentDate = new Date();
			year = year && (year >= 0 && year <= 9999) ? year : currentDate.getFullYear();
			month = month && (month >= 1 && month <= 12) ? (month - 1) : 11;
			day = day && (day >= 1 && day <= 31) ? day : 31;
			return new Date(year, month, day, 23, 59, 59, 999).getTime();
		}
	}
}

module.exports = new NumberUtil();