import moment from "moment";

const ME =  {
	publicAPI: "/",
	froalaConfig: {
		imageUploadURL: "/api/froala/images",
		fileUploadURL: "/api/froala/files",
		videoUploadURL: "/api/froala/videos"
	},
	isset: function (fn) {
	    var value;
	    try {
	        value = fn();
	    } catch (e) {
	        value = undefined;
	    } finally {
	        return value !== undefined;
	    }
	},
	formatDayMonthYear: function(milisecond) {
		var date = new Date(milisecond);
		return date.getDate() + "/" + (date.getMonth()  + 1)+ "/" + date.getFullYear();
	},
	filterQuestionsBaseOnId: function(parentQuestions, subQuestions) {
		return parentQuestions.filter(function(question) {
			var questionFilter = subQuestions.filter(function(subQuestion){
				return question._id == subQuestion._id;
			});
			return questionFilter.length == 0;
		});
	},
	firstDateOfCurrentYear: function() {
		var currentYear = new Date().getFullYear();
		return moment("01/01/" + currentYear, "DD/MM/YYYY").toDate();
	},
	lastDateOfCurrentYear: function() {
		var currentYear = new Date().getFullYear();
		return moment("31/12/" + currentYear, "DD/MM/YYYY").toDate();
	}
}
export { ME };