import dispatcher from "../dispatcher/dispatcher.js";

export function getAll() {
	dispatcher.dispatch({
		type: "QUESTION_GET_ALL",
		question: [
			{
	    		tags:"English",
	    		content: "What's your name?",
	    		hint: "The name your parent give you",
	    		type: "FREE_TEXT",
	    		answers: [],
	    	},
	    	{
	    		tags:"Technical",
	    		content: "What's your favorite language?",
	    		hint: "The name your parent give you",
	    		type: "FREE_TEXT"
	    	}
		]
	});
}

export function create(question) {
	dispatcher.dispatch({
		type: "QUESTION_CREATE",
		question,
	});
}

export function deleteById(questionId) {

}

export function update(question) {

}