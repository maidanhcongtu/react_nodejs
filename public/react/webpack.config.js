var webpack = require("webpack");
var path = require("path");


module.exports = {
	entry: {
		"bundle":"./main.jsx",
		//"bundle.min":"./main.jsx"
	},
	output: {
		path: "prod",
		publicPath: "/public/react/prod",
		filename: "[name].js",
	},
	module : {
		loaders: [
			{
				loader: "babel",
				exclude: /(node_modules | bower_components)/,
				query: {
					presets: ["react","es2015"],
					compact: false
				}
			},
            {	test: /\.css$/, 
            	loaders: ["style","css"] 
            },
            {
			    test: /\.(jpg|jpeg|gif|png|svg)$/,
			    exclude: /node_modules/,
			    loader:'url-loader?limit=10000&name=/images/[name].[ext]'
			},
			{
			    test: /\.(eot|woff|woff2|ttf|svg)(\?\S*)?$/,
			    loader: 'url-loader?limit=10000&name=/fonts/[name].[ext]'
			    //loader: 'url-loader?limit=10000&name=/prod/fonts/[sha512:hash:base64:7].[ext]' //hash file name
			}
		]
	},
	plugins: [
	    new webpack.ProvidePlugin({
	      	jQuery: 'jquery',
	      	$: 'jquery',
	      	jquery: 'jquery'
	    }),
	    //minimize code
	 // 		new webpack.DefinePlugin({
		//   	'process.env': {
		//     	NODE_ENV: JSON.stringify('production')
		//   	}
		// }),
		// new webpack.optimize.UglifyJsPlugin()
	]
}