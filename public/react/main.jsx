import "./node_modules/font-awesome/css/font-awesome.css";
import "./node_modules/react-datetime/css/react-datetime.css";
import "./node_modules/bootstrap/dist/css/bootstrap.css";
import "./asserts/styles/main.css";

import moment from "moment";
import "./node_modules/bootstrap/dist/js/bootstrap.js";

import React from "react";
import ReactDOM from "react-dom";
import { Router, Route, Link, browserHistory } from 'react-router';
import axios from 'axios';

//default config
axios.defaults.baseURL = 'http://localhost:3000/';
// axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';



{ /*admin com*/ }
	import AdminTemplateComponent from "./components/admin/AdminTemplateComponent.jsx";
	import AdminQuestionComponent from "./components/admin/AdminQuestionComponent.jsx";
	import AdminExaminationComponent from "./components/admin/AdminExaminationComponent.jsx";
	import AdminExaminationPartComponent from "./components/admin/AdminExaminationPartComponent.jsx";

{ /*user com*/ }
	import DefaultTemplateComponent from "./components/DefaultTemplateComponent.jsx";
	import ListCarouselProductComponent from "./components/ListCarouselProductComponent.jsx";
	import ProductDetailComponent from "./components/ProductDetailComponent.jsx";


{/*group user page*/}
	class HomePage extends React.Component {
		render () {
			return (
				<DefaultTemplateComponent children = {<ListCarouselProductComponent />}/>
			);
		}
	};

	class ProductDetailPage extends React.Component {

		render () {
			return (
				<DefaultTemplateComponent children = {<ProductDetailComponent params = {this.props.params}/>}/>
			);
		}

	};

{/*group admin page*/}
	class AdminQuestionlPage extends React.Component {
		render () {
			return (
				<AdminTemplateComponent children = {<AdminQuestionComponent params = {this.props.params}/>}/>
			);
		}
	};

	class AdminExaminationPartPage extends React.Component {
		render () {
			return (
				<AdminTemplateComponent children = {<AdminExaminationPartComponent params = {this.props.params}/>}/>
			);
		}
	};

	class AdminExaminationPage extends React.Component {
		render () {
			return (
				<AdminTemplateComponent children = {<AdminExaminationComponent params = {this.props.params}/>}/>
			);
		}
	};

ReactDOM.render((
  <Router history={browserHistory}>
    <Route path="/" component={HomePage} />
    <Route path="/san-pham/:productKey" component={ProductDetailPage} />

	<Route path="/admin/questions" component={AdminQuestionlPage} />    
	<Route path="/admin/exam-parts" component={AdminExaminationPartPage} />    
	<Route path="/admin/exams" component={AdminExaminationPage} />    
  </Router>
), document.getElementById('app'));
