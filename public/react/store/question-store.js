import {EventEmitter} from "Events";
import dispatcher from "../dispatcher/dispatcher.js";

class QuestionStore extends EventEmitter {
	constructor() {
		super();
	}

	getAll() {
		return [
			{
	    		tags:"English",
	    		content: "What's your name?",
	    		hint: "The name your parent give you",
	    		type: "FREE_TEXT",
	    		answers: [],
	    	},
	    	{
	    		tags:"Technical",
	    		content: "What's your favorite language?",
	    		hint: "The name your parent give you",
	    		type: "FREE_TEXT"
	    	}
		];
	}

	handleActions(action) {
		console.log("hellow action", action);
	}
}

const questionStore = new QuestionStore();
dispatcher.register(questionStore.handleActions.bind(questionStore));

export default questionStore;