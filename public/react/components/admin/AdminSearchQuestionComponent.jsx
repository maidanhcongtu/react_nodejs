import React from 'react';
import QuestionResource from '../../resources/question-resource.js';
import {ME} from '../../helpers/me.js';
import DateTime from 'react-datetime';
import MyInputTagsComponent from '../common/MyInputTagsComponent.jsx';

const questionTypes = [
	{value: 'ALL', name: 'Tất cả'},
	{value: 'ONE_CHOICE', name: 'One choice'},
	{value: 'MULTIPLE_CHOICE', name: 'Multiple choice'},
	{value: 'FREE_TEXT', name: 'Text'},
	{value: 'GROUP', name: 'Question group'}
];

export default class AdminSearchQuestionComponent extends React.Component {
	
	constructor() {
	    super();
	    this.state = {
	    	questionsFilterResult: [],
	    	filterBy: {
	    		tags: [],
	    		type: "ALL",
	    		fromDate: ME.firstDateOfCurrentYear(),
	    		toDate: ME.lastDateOfCurrentYear()
	    	},
	    	showEmptyResult: false
	    }
	}

	/*USER EVENT*/
	/*EVENT FOR FILTER*/
		handleChangeTags(tags) {
			let questionFilter = this.state.filterBy;
			questionFilter.tags = tags;
			this.setState({
				filterBy: questionFilter
			});
		}

		handleChangeFromDate(momentObject) {
			let questionFilter = this.state.filterBy;
			questionFilter.fromDate = momentObject.toDate();

			this.setState({
				filterBy: questionFilter
			});
		}

		handleChangeToDate(momentObject) {
			let questionFilter = this.state.filterBy;
			questionFilter.toDate = momentObject.toDate();

			this.setState({
				filterBy: questionFilter
			});
		}

		filterQuestion() {
			var that = this;
			var param = this.state.filterBy;

			QuestionResource.getByTagsAndTypeAndYearMonthDay(param.tags, param.type, param.fromDate.getTime(), param.toDate.getTime()).then(function(data) {
				var question = ME.filterQuestionsBaseOnId(data.data, that.props.selectedQuestions);
				if(question.length == 0) {
					var showEmptyResult = true;
				} else {
					var showEmptyResult = false;
				}
				that.setState({
					questionsFilterResult : question,
					showEmptyResult: showEmptyResult
				});
			});
		}

		handleChangeOnQuestionType(event) {
			let questionFilter = this.state.filterBy;
			questionFilter.type = event.target.value;
			this.setState({
				filterBy: questionFilter
			});
		}

	/*EVENT PARENT*/
		addQuestion(question, event) {
			this.props.updateSelectedQuestion(question);
			//update remain question
			var newQuestionFilterResult = this.state.questionsFilterResult.filter(function(val, idx){
				return val._id != question._id;
			});
			this.setState({
				questionsFilterResult : newQuestionFilterResult
			});
		}
	/*COMPONENT EVENT*/
		componentWillMount() {
		}

		componentDidMount() {
		}

		componentDidUpdate() {
		}

	render () {


		var that = this;
		return (
			<div className="admin-search-question-com">
				<label>Tìm câu hỏi</label>
				<div>
					<div className="asqc-form">
						<div>
							<MyInputTagsComponent value={this.state.filterBy.tags} placeholder="Nhãn" onChange={this.handleChangeTags.bind(this)} autoComplete={true} tagsDatalist={this.props.tagsQuestion}/>
						</div>

						<div>
							<select className="form-control" value={this.state.filterBy.type} onChange={this.handleChangeOnQuestionType.bind(this)}>
								{questionTypes.map(function(questionType) {
									return <option value={questionType.value} key={questionType.value}>{questionType.name}</option>;
								})}
							</select>
						</div>

						<div className="asqc-group-date">
							<div>
								<DateTime dateFormat="DD/MM/YYYY" timeFormat={false} value={this.state.filterBy.fromDate} closeOnSelect={true} onChange={this.handleChangeFromDate.bind(this)} />
							</div>

							<div>
								<DateTime dateFormat="DD/MM/YYYY" timeFormat={false} value={this.state.filterBy.toDate} closeOnSelect={true} onChange={this.handleChangeToDate.bind(this)}/>
							</div>
						</div>

						<div>
							<input type="button" value="Tìm" className="btn btn-primary" onClick={this.filterQuestion.bind(this)}/>
						</div>

					</div>

					<div className="asqc-item-question-w">
						{this.state.questionsFilterResult.map(function(question, idx){
							return (
								<div className="asqc-item-question" key={idx}>
									<div dangerouslySetInnerHTML={{__html: question.content}} className="asqc-q-content"></div>
									<div className="asqc-q-type">{question.type}</div>
									<div className="asqc-q-action">
										<i className="fa fa-clock-o" aria-hidden="true" title={ME.formatDayMonthYear(question.createdAt)}></i>
										<i className="fa fa-plus-circle" aria-hidden="true" onClick={that.addQuestion.bind(that, question)}></i>
									</div>

								</div>
							);
						})}

						{this.state.showEmptyResult && 
							<div className="asqc-item-question">
								Không có câu hỏi nào
							</div>
						}
						
					</div>
				</div>
				
				
			</div>
		);
	}
}