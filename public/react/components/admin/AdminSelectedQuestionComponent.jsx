import React from 'react';
import QuestionResource from '../../resources/question-resource.js';
import {ME} from '../../helpers/me.js';

export default class AdminSelectedQuestionComponent extends React.Component {
	
	constructor() {
	    super();
	    this.state = {
	    	selectedQuestion: []
	    }
	}

	/*USER EVENT*/
	/*EVENT FOR FILTER*/

	/*EVENT PARENT*/
		removeQuestion(idQuestion, event) {
			this.props.removeSelectedQuestion(idQuestion);
		}
	/*COMPONENT EVENT*/
		componentWillMount() {
		}

		componentDidMount() {
		}

		componentDidUpdate() {
		}

	render () {
		var that = this;
		return (
			<div className="admin-selected-question-com">
				<label>Câu hỏi được chọn</label>
				<table className="table">
					<thead>
						<tr>
							<th>Nội dung</th>
							<th>Tags</th>
							<th>Loại</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						{this.props.selectedQuestions.map(function(question, idx){
							return (
								<tr key={idx}>
									<td>
										<div dangerouslySetInnerHTML={{__html: question.content}}></div>
									</td>
									<td>{question.tags.join(";")}</td>
									<td>{question.type}</td>
									<td>
										<span className="fa fa-minus-circle" aria-hidden="true" onClick={that.removeQuestion.bind(that, question._id)}></span>
									</td>
								</tr>
							);
						})}
					</tbody>
					{this.props.selectedQuestions.length == 0 && 
						<tfoot>
							<tr><td colSpan="10">Không có câu hỏi nào</td></tr>
						</tfoot>
					}
				</table>
			</div>
		);
	}
}