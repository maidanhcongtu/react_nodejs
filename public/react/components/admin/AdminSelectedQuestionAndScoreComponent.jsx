import React from 'react';

export default class AdminSelectedQuestionAndScoreComponent extends React.Component {
	
	constructor() {
	    super();
	    this.state = {
	    	selectedQuestion: []
	    }
	}

	/*USER EVENT*/
	/*EVENT FOR FILTER*/

	/*EVENT PARENT*/
		removeQuestion(idQuestion, event) {
			this.props.removeSelectedQuestionScore(idQuestion);
		}

		handleChangeScore(idx, event) {
			this.props.onChangeScore(idx, event.target.value);
		}
	/*COMPONENT EVENT*/
		componentWillMount() {
		}

		componentDidMount() {
		}

		componentDidUpdate() {
		}

	render () {
		var that = this;
		var totalScore = this.props.selectedQuestionScores.map(function(questionScore, idx){
			return questionScore.score;
		}).reduce((acc, cur) => parseInt(acc) + parseInt(cur), 0);
		return (
			<div className="admin-selected-question-com">
				<label>Câu hỏi được chọn</label>
				<div>
					<label className={totalScore == 0 ? 'hide' :''}>Tổng điểm: {totalScore}</label>
				</div>
				<table className="table">
					<thead>
						<tr>
							<th>Điểm</th>
							<th>Nội dung</th>
							<th>Tags</th>
							<th>Loại</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						{this.props.selectedQuestionScores.map(function(questionScore, idx){
							return (
								<tr key={idx}>
									<td>
										<input type="number" value={questionScore.score} onChange={that.handleChangeScore.bind(that, idx)} className="form-control w-60"/>
									</td>
									<td>
										<div dangerouslySetInnerHTML={{__html: questionScore.question.content}}></div>
									</td>
									<td>{questionScore.question.tags.join(";")}</td>
									<td>{questionScore.question.type}</td>
									<td>
										<span className="fa fa-minus-circle" aria-hidden="true" onClick={that.removeQuestion.bind(that, questionScore.question._id)}></span>
									</td>
								</tr>
							);
						})}
					</tbody>
					{this.props.selectedQuestionScores.length == 0 && 
						<tfoot>
							<tr><td colSpan="10">Không có câu hỏi nào</td></tr>
						</tfoot>
					}
				</table>
			</div>
		);
	}
}