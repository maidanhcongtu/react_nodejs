import React from 'react';
import {ME} from '../../helpers/me.js';
import QuestionResource from '../../resources/question-resource.js';
import TagResource from '../../resources/tag-resource.js';
import QuestionStore from '../../store/question-store.js';
import * as QuestionAction from '../../actions/question-action.js';
import AdminAnswerOneChoiceComponent from './AdminAnswerOneChoiceComponent.jsx';
import AdminSearchQuestionComponent from './AdminSearchQuestionComponent.jsx';
import AdminSelectedQuestionComponent from './AdminSelectedQuestionComponent.jsx';
import MyInputTagsComponent from '../common/MyInputTagsComponent.jsx';

// Require Editor JS files.
require("froala-editor/js/froala_editor.pkgd.min.js");
// Require Editor CSS files.
require("froala-editor/css/froala_style.min.css");
require("froala-editor/css/froala_editor.pkgd.min.css");

var FroalaEditor = require('react-froala-wysiwyg');




const questionTypes = [
	{value: 'ONE_CHOICE', name: 'One choice'},
	{value: 'MULTIPLE_CHOICE', name: 'Multiple choice'},
	{value: 'FREE_TEXT', name: 'Text'},
	{value: 'GROUP', name: 'Question group'}
];

const styleWrapper = {
	margin: 'auto',
}

const styleForm = {
	maxWidth: 600,
	margin: 'auto'
}

export default class AdminQuestionComponent extends React.Component {
	
	constructor() {
	    super();
	    this.state = {
	    	question: this.emptyQuestion(),
	    	questions: [],
	    	editMode: false,
	    	tagsDatalist: [],
	    };
	}

	/*HANDLE ACTION*/
		handleSubmit(event) {
			var that = this;
			event.preventDefault();
			//call api here
			//save new one
			if(!this.state.editMode) {
				QuestionResource
					.save(this.state.question)
					.then(function(data){
						var questions = that.state.questions;
						questions.push(data.data);
						//update question
						that.setState({
							question: that.emptyQuestion(), 
							questions:questions
						});
						that.updateQuestionTags();
						that.doFocusOnTags();
					});
			} else {
				//update question
				QuestionResource
					.update(this.state.question)
					.then(function(data){
						var questions = that.state.questions;
						for(var i in questions) {
							if(questions[i]._id == data.data._id) {
								questions[i] = data.data;
								break;
							}
						}
						//update question
						that.setState({
							question: that.emptyQuestion(), 
							questions:questions,
							editMode: false
						});
						that.updateQuestionTags();
						that.doFocusOnTags();
					});
			}
		}

		deleteQuestion(questionID) {
			let that = this;
			let questionsState = this.state.questions;
			QuestionResource.deleteById(questionID).then(function(data){
				if(ME.isset(() => data.data._id)) {
					let id = data.data._id;
					const newArray = questionsState.filter(obj => obj._id != id);
					that.setState({
						questions: newArray
					});
				}
			});
		}
		editQuestion(question) {
			var cloneQuestion = JSON.parse(JSON.stringify(question));
			this.setState({question: cloneQuestion, editMode: true});
			this.doFocusOnTags();
		}

		cancelUpdate() {
			this.setState({
				editMode: false,
				question: this.emptyQuestion()
			});
		}

	/*HANDLE DATA*/
		handleChangeOnQuestionType(event) {
			let stateQuestion = this.state.question;
			stateQuestion.type = event.target.value; 
			//update question
			this.setState({question: stateQuestion});
		}

		handleChangeTags(tags) {
			let stateQuestion = this.state.question;
			stateQuestion.tags = tags;
			//update question
			this.setState({question: stateQuestion});
		}

		handleChangeContent(value) {
			let stateQuestion = this.state.question;
			stateQuestion.content = value;
			//update question
			this.setState({question: stateQuestion});
		}

		handleChangeHint(event) {
			let value = event.target.value;
			let stateQuestion = this.state.question;
			stateQuestion.hint = value;
			//update question
			this.setState({question: stateQuestion});
		}

	/*HANDLE CHILD COMPONENT CALL BACK*/
		updateAnswers(answers) {
			let stateQuestion = this.state.question;
			stateQuestion.answers = answers;
			this.setState({question: stateQuestion});
		}

		updateSelectedQuestion(questionSelectedFromSearching) {
			let question = this.state.question;
			question.childrenQuestions.push(questionSelectedFromSearching);
			this.setState({question: question});
		}

		removeSelectedQuestion(questionId) {
			let question = this.state.question;
			question.childrenQuestions = question.childrenQuestions.filter(function(val, idx) {
				return questionId != val._id;
			});
			this.setState({question: question});
		}

	/*API OCMPONENT METHOD*/
	componentDidMount() {
		var that = this;
		//get all question
		QuestionResource.getAll().then(function(data){
			//update questions
			that.setState({questions: data.data});
		});

		this.updateQuestionTags();
		this.doFocusOnTags();
	}

	/*PRIVATE METHOD*/
	updateQuestionTags() {
		var that = this;
		// get all tag belong to question
		TagResource.getByType("question").then(function(data){
			if(data.data != null) {
				that.setState({tagsDatalist: data.data.tags});
			}
		})
	}

	emptyQuestion() {
		return {
			tags:[],
			type: questionTypes[0].value,
			content: "",
			hint: "",
			answers: [
				{
					content: "",
					correctAnswer: false
				},
				{
					content: "",
					correctAnswer: false
				},
				{
					content: "",
					correctAnswer: false
				},
				{
					content: "",
					correctAnswer: false
				}
			],
			//this for select question when create group question
			childrenQuestions: []
		}
	}

	doFocusOnTags() {
		var questionTags = this.refs.questionTags;
		questionTags.refs.refMyInputTags.focus();
	}

	render () {
		var that = this;
		return (
			<div style={styleWrapper}>
				<form onSubmit={this.handleSubmit.bind(this)} style={styleForm}>

					<div className="form-group">
						<label htmlFor="typeOfQuestion">Loại Câu Hỏi</label>
						<select className="form-control" id="typeOfQuestion" value={this.state.question.type} onChange={this.handleChangeOnQuestionType.bind(this)}>
							{questionTypes.map(function(questionType) {
								return <option value={questionType.value} key={questionType.value}>{questionType.name}</option>;
							})}
						</select>
					</div>

					<div className="form-group">
						<label htmlFor="tags">Tags câu hỏi</label>
						<MyInputTagsComponent value={this.state.question.tags} ref="questionTags" onChange={this.handleChangeTags.bind(this)} autoComplete={true} tagsDatalist={this.state.tagsDatalist}/>
					</div>

					<div className="form-group">
						<label htmlFor="questionContent">Nội dung câu hỏi</label>
						{<FroalaEditor
						  tag='textarea'
						  config={ME.froalaConfig}
						  model={this.state.question.content}
						  onModelChange={this.handleChangeContent.bind(this)} />}
					</div>

					<div className="form-group">
						<label htmlFor="hintQuestion">Gợi ý</label>
						<textarea className="form-control" id="hintQuestion" value={this.state.question.hint} data-question-property="hint" onChange={this.handleChangeHint.bind(this)}></textarea>
					</div>

					<div>
						{
							this.state.question.type == "GROUP" && <AdminSelectedQuestionComponent selectedQuestions = {this.state.question.childrenQuestions} removeSelectedQuestion={this.removeSelectedQuestion.bind(this)}/>
						}
					</div>

					<div>
						{
							this.state.question.type == "GROUP" ?
								<AdminSearchQuestionComponent selectedQuestions={this.state.question.childrenQuestions} updateSelectedQuestion={this.updateSelectedQuestion.bind(this)} tagsQuestion={this.state.tagsDatalist}/> : 
								this.state.question.type != "FREE_TEXT" && <AdminAnswerOneChoiceComponent multipleChoice={this.state.question.type == "MULTIPLE_CHOICE"} answers={this.state.question.answers} updateAnswers={this.updateAnswers.bind(this)}/>
						}
					</div>

					<div className="form-group">
						{
							!this.state.editMode ? 
							<button type="submit" className="btn btn-primary">Tạo câu hỏi</button> : 
							<span>
								<button type="button" className="btn btn-danger" onClick={this.cancelUpdate.bind(this)}>Hủy sửa</button> 
								<button type="submit" className="btn btn-primary">Sửa câu hỏi</button>
							</span>
						}
					</div>

				</form>

				<hr/>
				<table className="table">
					<thead>
						<tr>
							<th>Tags</th>
							<th>Nội dung</th>
							<th>Gợi ý</th>
							<th>Loại</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						{this.state.questions.map(function(question, idx){
							return (
								<tr key={idx}>
									<td>{question.tags.join(";")}
									</td>
									<td>
										<div dangerouslySetInnerHTML={{__html: question.content}}></div>
									</td>
									<td>{question.hint}</td>
									<td>{question.type}</td>
									<td>
										<span className="fa fa-minus-circle" aria-hidden="true" onClick={that.deleteQuestion.bind(that, question._id)}></span>
										<span className="fa fa-pencil-square" aria-hidden="true" onClick={that.editQuestion.bind(that, question)}></span>
									</td>
								</tr>
							);
						})}
					</tbody>
					{this.state.questions.length == 0 && 
						<tfoot>
							<tr><td colSpan="10">Không có câu hỏi nào</td></tr>
						</tfoot>
					}
				</table>
			</div>
		);
	}
}