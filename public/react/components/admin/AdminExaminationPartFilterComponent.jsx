import React from 'react';
import QuestionResource from '../../resources/question-resource.js';
import {ME} from '../../helpers/me.js';
import DateTime from 'react-datetime';

export default class AdminSearchQuestionComponent extends React.Component {
	
	constructor() {
	    super();
	    this.state = {
	    	questionsFilterResult: [],
	    	filterBy: {
	    		tags: "",
	  			title: "",
	    		fromDate: ME.firstDateOfCurrentYear(),
	    		toDate: ME.lastDateOfCurrentYear()
	    	}
	    }
	}

	/*USER EVENT*/
	/*EVENT FOR FILTER*/
		handleChangeTags(event) {
			let questionFilter = this.state.filterBy;
			questionFilter.tags = event.target.value;
			this.setState({
				filterBy: questionFilter
			});
		}

		handleChangeFromDate(momentObject) {
			let questionFilter = this.state.filterBy;
			questionFilter.fromDate = momentObject.toDate();

			this.setState({
				filterBy: questionFilter
			});
		}

		handleChangeToDate(momentObject) {
			let questionFilter = this.state.filterBy;
			questionFilter.toDate = momentObject.toDate();

			this.setState({
				filterBy: questionFilter
			});
		}

		filterQuestion() {
			var that = this;
			var param = this.state.filterBy;
			var tags = param.tags ? param.tags.split(";") : [];

			QuestionResource.getByTagsAndTypeAndYearMonthDay(tags, param.type, param.fromDate.getTime(), param.toDate.getTime()).then(function(data) {
				var question = ME.filterQuestionsBaseOnId(data.data, that.props.selectedQuestions);
				that.setState({
					questionsFilterResult : question
				});
			});
		}

		handleChangeOnQuestionType(event) {
			let questionFilter = this.state.filterBy;
			questionFilter.type = event.target.value;
			this.setState({
				filterBy: questionFilter
			});
		}

	/*EVENT PARENT*/
		addQuestion(question, event) {
			this.props.updateSelectedQuestion(question);
			//update remain question
			var questions = ME.filterQuestionsBaseOnId(this.state.questionsFilterResult, this.props.selectedQuestions);
				this.setState({
					questionsFilterResult : questions
				});
		}
	/*COMPONENT EVENT*/
		componentWillMount() {
		}

		componentDidMount() {
			var questionTypes = $.extend([], this.props.questionTypes);
			questionTypes.unshift({value: 'ALL', name: 'Tất cả'});
			this.setState({
				questionTypes: questionTypes
			});
		}

		componentDidUpdate() {
		}

	render () {


		var that = this;
		return (
			<div className="admin-search-question-com">
				<label>Tìm câu hỏi</label>
				<div>
					<div className="asqc-form">
						<div>
							<input className="form-control" type="text" value={this.state.filterBy.tags} placeholder="Nhãn" onChange={this.handleChangeTags.bind(this)}/>
						</div>

						<div>
							<select className="form-control" value={this.state.filterBy.type} onChange={this.handleChangeOnQuestionType.bind(this)}>
								{this.state.questionTypes.map(function(questionType) {
									return <option value={questionType.value} key={questionType.value}>{questionType.name}</option>;
								})}
							</select>
						</div>

						<div className="asqc-group-date">
							<div>
								<DateTime dateFormat="DD/MM/YYYY" timeFormat={false} value={this.state.filterBy.fromDate} closeOnSelect={true} onChange={this.handleChangeFromDate.bind(this)} />
							</div>

							<div>
								<DateTime dateFormat="DD/MM/YYYY" timeFormat={false} value={this.state.filterBy.toDate} closeOnSelect={true} onChange={this.handleChangeToDate.bind(this)}/>
							</div>
						</div>

						<div>
							<input type="button" value="Tìm" className="btn btn-primary" onClick={this.filterQuestion.bind(this)}/>
						</div>

					</div>

					<div className="asqc-item-question-w">
						{this.state.questionsFilterResult.map(function(question, idx){
							return (
								<div className="asqc-item-question" key={idx}>
									<div dangerouslySetInnerHTML={{__html: question.content}} className="asqc-q-content"></div>
									<div className="asqc-q-type">{question.type}</div>
									<div className="asqc-q-action">
										<i className="fa fa-clock-o" aria-hidden="true" title={ME.formatDayMonthYear(question.createdAt)}></i>
										<i className="fa fa-plus-circle" aria-hidden="true" onClick={that.addQuestion.bind(that, question)}></i>
									</div>

								</div>
							);
						})}
						
					</div>
				</div>
				
				
			</div>
		);
	}
}