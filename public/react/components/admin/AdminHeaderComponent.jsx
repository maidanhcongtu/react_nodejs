import React from 'react';
import Logo from "../.././asserts/images/logo.jpg";
import { Link } from 'react-router';

export default class HeaderComponent extends React.Component {
	
	constructor() {
	    super();
	    this.state = { showSubMenu : false };
	}

	render () {
		return (
			<nav className="navbar navbar-default header-admin-container">
			    <div className="container-fluid">

			        <div className="navbar-header">
			            <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			            <span className="sr-only">Toggle navigation</span>
			            <span className="icon-bar"></span>
			            <span className="icon-bar"></span>
			            <span className="icon-bar"></span>
			            </button>
			            <a className="navbar-brand" href="/public">Trang Chủ</a>
			        </div>
			        
			        <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			            <ul className="nav navbar-nav">
			                <li className="dropdown">
			                    <a href="" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Quản lý <span className="caret"></span></a>
			                    <ul className="dropdown-menu">
			                        <li>
			                        	<Link to={"/admin/questions"} activeClassName="active">Câu Hỏi</Link>
			                        </li>
			                        <li>
			                        	<Link to={"/admin/exam-parts"} activeClassName="active">Tạo Phần Thi</Link>
			                        </li>
			                        <li>
			                        	<Link to={"/admin/exams"} activeClassName="active">Tạo Bài Thi</Link>
			                        </li>
			                    </ul>
			                </li>
			                <li><a href="admin/user">Quản lí user</a></li>
			                <li><a href="admin/setting">Cấu hình</a></li>
			            </ul>
			            <ul className="nav navbar-nav navbar-right">
			                <li><a href="" title="Đăng xuất">Đăng xuất</a></li>
			            </ul>
			        </div>
			    </div>
			</nav>
		);
	}
}