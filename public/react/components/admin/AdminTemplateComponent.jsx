import React from 'react';

import AdminHeaderComponent from "./AdminHeaderComponent.jsx";

export default class DefaultTemplateComponent extends React.Component {
	
	render () {
		return (
			<div className="app-container">
				
				<div id="header-component">
					<AdminHeaderComponent />
				</div>

				<div>
					{this.props.children}
				</div>

			</div>
		);
	}
}