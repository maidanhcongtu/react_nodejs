import React from 'react';

export default class AdminAnswerOneChoiceComponent extends React.Component {
	
	constructor() {
	    super();
	    this.state = {
	    	inputFocusRef: ""
	    }
	}

	addAnswerItem(idx, event) {
		var arr = this.props.answers;
		arr.splice(idx + 1, 0, this.defaultAnswerObject());
		this.props.updateAnswers(arr);
		var ref = "inputRef" + (parseInt(idx) + 1);
		this.setState({
			inputFocusRef: ref
		});
	}

	removeAnswerItem(idx, event) {
		var arr = this.props.answers;
		arr.splice(idx, 1);
		this.props.updateAnswers(arr);
	}

	handleChangeAnswerContent(idx, event) {
		var arr = this.props.answers;
		arr[idx].content = event.target.value;
		this.props.updateAnswers(arr);
	}

	handleSetCorrectAnswer(idx, event) {
		//set false for all answer
		var arr = this.props.answers;
		arr.forEach(function(item){
			item.correctAnswer = false;
		});
		arr[idx].correctAnswer = true;
		this.props.updateAnswers(arr);
	}

	handleChangeMultiChoiceAnswer(idx, event) {
		var arr = this.props.answers;
		arr[idx].correctAnswer = event.target.checked;
		this.props.updateAnswers(arr);
	}

	defaultAnswerObject() {
		return {
			content: "",
			correctAnswer: false
		};
	}

	componentWillMount() {
	}

	componentDidMount() {
	}

	componentDidUpdate() {
		var ref = this.refs[this.state.inputFocusRef];
		if(ref) {
			ref.focus();
			//reset ref
			this.setState({
				inputFocusRef: ""
			})
		}
	}

	render () {
		var that = this;
		var showRemoveIcon = this.props.answers.length > 1 ? true : false;
		var multipleChoice = this.props.multipleChoice ? true : false;
		return (
			<div>
				<label>Nhập đáp án</label>
				{that.props.answers.map(function(answer, idx){
					return (
						<div className="form-group" key={idx}>
							<div className="col-md-1">
								{
									multipleChoice ? 
									(<input type="checkbox" checked={answer.correctAnswer} name="multiCorrectAnswer" onChange={that.handleChangeMultiChoiceAnswer.bind(that, idx)}/>) : 
									(<input type="radio" name="correctAnswer" checked={answer.correctAnswer} onChange={that.handleSetCorrectAnswer.bind(that, idx)}/>)
								}
							</div>
							<div className="col-md-9">
								<input className="form-control" id="tags" autoComplete="false" ref={"inputRef" + idx} value = {answer.content} onChange={that.handleChangeAnswerContent.bind(that,idx)}/>
							</div>
							<div className="col-md-2">
								<span className="fa fa-plus-circle" aria-hidden="true" onClick={that.addAnswerItem.bind(that, idx)}></span>
								{showRemoveIcon && <span className="fa fa-minus-circle" aria-hidden="true" onClick={that.removeAnswerItem.bind(that, idx)}></span>}
							</div>
							<div className="clearfix"></div>
						</div>
					);
				})}
				
			</div>
		);
	}
}