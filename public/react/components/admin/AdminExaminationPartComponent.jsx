import React from 'react';
import {ME} from '../../helpers/me.js';

import ExamPartResource from '../../resources/exam-part-resource.js';
import TagResource from '../../resources/tag-resource.js';

import MyInputTagsComponent from '../common/MyInputTagsComponent.jsx';
import AdminSearchQuestionComponent from './AdminSearchQuestionComponent.jsx';
import AdminSelectedQuestionAndScoreComponent from './AdminSelectedQuestionAndScoreComponent.jsx';


const styleWrapper = {
	margin: 'auto',
	maxWidth: 800
}

export default class AdminExaminationPartComponent extends React.Component {
	
	constructor() {
	    super();
	    this.state = {
	    	parts: [],
	    	part: this.emptyPart(),
	    	tagsExamPartDatalist: [],
	    	tagsQuestionDatalist: [],
	    	editMode: false
	    };
	}

	/*HANDLE ACTION*/
		handleSubmit(event) {
			var that = this;
			event.preventDefault();
			//call api here
			//save new one
			if(!this.state.editMode) {
				ExamPartResource.save(this.state.part).then(function(data){
					var parts = that.state.parts;
					parts.push(data.data);
					that.setState({parts: parts, part: that.emptyPart()});
					that.updateExmaPartTags();
					that.doFocusOnTags();
				});
			} else {
				//update examp part
				ExamPartResource
					.update(this.state.part)
					.then(function(data){
						var parts = that.state.parts;
						for(var i in parts) {
							if(parts[i]._id == data.data._id) {
								parts[i] = data.data;
								break;
							}
						}
						//update question
						that.setState({
							part: that.emptyPart(), 
							parts: parts
						});
						that.updateExmaPartTags();
						that.doFocusOnTags();
					});
				this.setState({editMode: false});
				
			}
		}

		deletePart(partId) {
			var that = this;
			ExamPartResource.deleteById(partId).then(function(data){
				if(ME.isset(() => data.data._id)) {
					let id = data.data._id;
					const newArray = that.state.parts.filter(obj => obj._id != id);
					that.setState({
						parts: newArray
					});
				}
			});
		}

		editPart(part) {
			var clonePart = $.extend({}, part);
			this.setState({
				part: clonePart,
				editMode: true
			});
			this.doFocusOnTags();
		}

		cancelUpdate() {
			this.setState({
				part: this.emptyPart(),
				editMode: false
			});
			this.doFocusOnTags();
		}

		

	/*HANDLE DATA*/
		handleChangeInputTags(tags) {
			let part = this.state.part;
			part.tags = tags;
			this.setState({part: part});
		}

		handleChangeTitle(event) {
			let part = this.state.part;
			part.title = event.target.value;
			this.setState({part: part});
		}

		handleChangeDescription(event) {
			let part = this.state.part;
			part.description = event.target.value;
			this.setState({part: part});
		}

	/*HANDLE CHILD COMPONENT CALL BACK*/
		updateSelectedQuestion(questionSelectedFromSearching) {
			let part = this.state.part;
			var questionScore = {
				question: questionSelectedFromSearching,
				score: 1
			};
			part.questionScores.push(questionScore);
			this.setState({part: part});
		}

		removeSelectedQuestionScore(questionId) {
			let part = this.state.part;
			part.questionScores = part.questionScores.filter(function(val, idx) {
				return questionId != val.question._id;
			});
			this.setState({part: part});
		}

		onChangeScore(idx, value) {
			var part = this.state.part;
			part.questionScores[idx].score = value;
			this.setState({part:part});
		}

	emptyPart() {
		return {
			tags: [],
			title: "",
			description: "",
			questionScores: []
		}
	}

	componentDidMount() {
		var that = this;
		// get all exam part
		ExamPartResource.getAll().then(function(data){
			that.setState({parts: data.data});
		});

		
		this.updateExmaPartTags();
		this.updateQuestionTags();
		this.doFocusOnTags();
	}

	updateExmaPartTags() {
		var that = this;
		// get all tag belong to exam-part
		TagResource.getByType("exam-part").then(function(data){
			if(data.data != null) {
				that.setState({tagsExamPartDatalist: data.data.tags});
			}
		})
	}

	updateQuestionTags() {
		var that = this;
		// get all tag belong to question
		TagResource.getByType("question").then(function(data){
			if(data.data != null) {
				that.setState({tagsQuestionDatalist: data.data.tags});
			}
		})
	}

	doFocusOnTags() {
		var tagsPart = this.refs.tagsPart;
		tagsPart.refs.refMyInputTags.focus();
	}

	render () {
		var that = this;
		return (
			<div style={styleWrapper}>
				<form onSubmit={this.handleSubmit.bind(this)}>

					<div className="form-group">
						<label htmlFor="tagsPart">Tags phần thi</label>
						<MyInputTagsComponent value={this.state.part.tags} ref="tagsPart" onChange={this.handleChangeInputTags.bind(this)} autoComplete={true} tagsDatalist={this.state.tagsExamPartDatalist}/>
					</div>

					<div className="form-group">
						<label>Tiêu đề phần thi</label>
						<input className="form-control" type="text" id="titlePart" value={this.state.part.title} onChange={this.handleChangeTitle.bind(this)}/>
					</div>

					<div className="form-group">
						<label htmlFor="desPart">Mô tả phần thi</label>
						<textarea className="form-control" id="desPart" value={this.state.part.description} onChange={this.handleChangeDescription.bind(this)}>
						</textarea>
					</div>

					<div>
						<AdminSelectedQuestionAndScoreComponent selectedQuestionScores={this.state.part.questionScores} removeSelectedQuestionScore={this.removeSelectedQuestionScore.bind(this)} onChangeScore={this.onChangeScore.bind(this)}/>
					</div>

					<div>
						<AdminSearchQuestionComponent selectedQuestions={this.state.part.questionScores.map(function(val, idx){
							return val.question; 
						})} updateSelectedQuestion={this.updateSelectedQuestion.bind(this)} tagsQuestion={this.state.tagsQuestionDatalist} /> 
					</div>

					<div>
						{
							!this.state.editMode ? 
							<button type="submit" className="btn btn-primary">Tạo phần thi</button> : 
							<span>
								<button type="button" className="btn btn-danger" onClick={this.cancelUpdate.bind(this)}>Hủy sửa</button> 
								<button type="submit" className="btn btn-primary">Sửa phần thi</button>
							</span>
						}
					</div>
				</form>

				<div>
					<table className="table">
						<thead>
							<tr>
								<th>Tags</th>
								<th>Tiêu đề</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							{this.state.parts.map(function(part, idx){
								return (
									<tr key={idx}>
										<td>{part.tags.join(";")}</td>
										<td>
											{part.title}
										</td>
										<td>
											<span className="fa fa-minus-circle" aria-hidden="true" onClick={that.deletePart.bind(that, part._id)}></span>
											<span className="fa fa-pencil-square" aria-hidden="true" onClick={that.editPart.bind(that, part)}></span>
										</td>
									</tr>
								);
							})}
						</tbody>
						{this.state.parts.length == 0 && 
							<tfoot>
								<tr><td colSpan="10">Không có phần thi nào</td></tr>
							</tfoot>
						}
					</table>
				</div>
			</div>
		);
	}
}