import React from 'react';
import {ME} from '../../helpers/me.js';

export default class MyInputTagsComponent extends React.Component {
	
	constructor() {
	    super();
	    this.state = {
	    	/*state for input*/
	    	enterValue: "",
	    	inputWidth: 80,
	    	eligibleDeleteLastItem: false, 
	    	focusedIn: false,
	    	/*state for auto complete*/
	    	tagsDatalist: [],
	    	indexSelected: -1
	    }
	}

	/*COMPONENT EVENT*/
		focusOnInputSearch(event) {
			this.refs["refMyInputTags"].focus();
		}
		
		removeTag(tag, event) {
			var tags = this.props.value;
			var index = tags.indexOf(tag);
			if(index != -1) {
				tags.splice(index, 1);
				this.props.onChange(tags);
			}
		}

		clickAddTag(tag, event) {
			this.addTag(tag);
		}

		handleOnBlur(event) {
			this.addTag(event.target.value);
			this.setState({focusedIn: false});
		}

		handleOnFocus(event) {
			this.updateTagsDataList(event.target.value);
			this.setState({focusedIn: true});
		}

		handleOnKeyDown(event) {
			var enterValue = this.state.enterValue;
			var eligibleDeleteLastItem = this.state.eligibleDeleteLastItem;
			var tags = this.props.value;

			var codeTabKey = 9;
			var codeBackSpace = 8;
			var codeArrowKeyDown = 40;
			var codeArrowKeyUp = 38;
			var codeKeyEnter = 13;

			if(event.keyCode == codeKeyEnter) {
				var indexSelected = this.state.indexSelected;
				if(indexSelected >= 0) {
					this.addTag(this.state.tagsDatalist[indexSelected]);
					this.updateTagsDataList("");
				} else if(enterValue.trim() != "") {
					this.addTag(enterValue);
				}
				event.preventDefault();
			}
			else if(event.keyCode == codeTabKey && enterValue.trim() != "") {
				event.preventDefault();
				this.addTag(enterValue);
				this.refs["refMyInputTags"].focus();
			} else if(event.keyCode == codeBackSpace && enterValue == "") {
				if(eligibleDeleteLastItem) {
					//delete last item
					tags.splice(-1,1);
					this.props.onChange(tags);
					//reset condition delete last item
					eligibleDeleteLastItem = false;
				} else {
					eligibleDeleteLastItem = true;
				}
				this.setState({eligibleDeleteLastItem});
			}

			//handle up arrow key
			if(event.keyCode == codeArrowKeyUp) {
				var indexSelected = this.state.indexSelected - 1;
				if(indexSelected < 0) {
					indexSelected = (this.state.tagsDatalist.length - 1);
				}
				this.setState({indexSelected});
				event.preventDefault();
			}

			//handle down arrow key
			if(event.keyCode == codeArrowKeyDown) {
				var indexSelected = this.state.indexSelected + 1;
				if(indexSelected >= this.state.tagsDatalist.length) {
					indexSelected = 0;
				}
				this.setState({indexSelected});
				event.preventDefault();
			}
		}

		handleOnKeyUp(event) {
			var enterValue = this.state.enterValue;

			//get length of enterValue
			var lengthWidth = enterValue.length * 7;//7 is width for 1 letter
			var inputWidth = this.state.inputWidth;
			if(lengthWidth > 80) {
				inputWidth = lengthWidth;
			} else {
				inputWidth = 80;
			}
			this.setState({inputWidth});
		}

		handleOnChangeInput(event) {
			this.updateTagsDataList(event.target.value);
		}

		mouseEnterItem(idx, event) {
			this.setState({indexSelected: idx});
		}

		mouseLeaveItem(event) {
			this.setState({indexSelected: -1});
		}

	/*PRIVATE FUNCTION*/
		addTag(tag) {
			var tags = this.props.value;
			var enterValue = tag;
			if(enterValue.length > 0 && tags.indexOf(enterValue) == -1) {
				tags.push(enterValue);
				this.props.onChange(tags);
				this.setState({enterValue: ""});
			}
			this.setState({eligibleDeleteLastItem : false});
		}

		renderClassItemAutoComplete(idx) {
			return "mytaginput-com-item-auto-complete " + (idx == this.state.indexSelected ? "my-selected" : ""); 
		}

		updateTagsDataList(currentValue) {
			var value =  currentValue;
			var tags = this.props.value;
			var indexSelected = -1;
			var tagsDatalist = [];
			if(this.props.autoComplete) {
				tagsDatalist = this.props.tagsDatalist.filter(function(val, idx) {
						return val.toLocaleLowerCase().indexOf(value.toLocaleLowerCase()) != -1 && tags.indexOf(val) == -1;
				});
				if(tagsDatalist.length > 0) {
					indexSelected = 0;
				}
			}
			this.setState({enterValue: value, tagsDatalist: tagsDatalist, indexSelected: indexSelected});
		}
		
	/*COMPONENT EVENT*/
		componentWillMount() {
		}

		componentDidMount() {
			this.setState({tagsDatalist: this.props.tagsDatalist});
		}

		componentDidUpdate() {
		}

	render () {
		var that = this;
		var inputStyle = {width: this.state.inputWidth};
		var eligibleDeleteLastItem = this.state.eligibleDeleteLastItem;
		var tagsLength = this.props.value.length;
		var isShowAutoCompletePanel = this.props.autoComplete && this.state.focusedIn && this.state.tagsDatalist.length > 0;
		return (
			<div className="mytaginput-com">
				<div className="mytaginput-com-content" onClick={this.focusOnInputSearch.bind(this)}>
					<div className="mytaginput-com-list-tages-selected">
						{this.props.value.map(function(val, idx){
							var styleClassTaged = "mytaginput-com-item-taged";
							if(eligibleDeleteLastItem && idx == (tagsLength - 1)) {
								styleClassTaged = styleClassTaged + " my-deleting"; 
							}
							return (
								<div className={styleClassTaged} key={idx}>{val} <i className="fa fa-times mytaginput-com-icon-delete" onClick={that.removeTag.bind(that, val)}></i>
								</div>
							);
						})}
						<div>
							<input type="text" ref="refMyInputTags" className="mytaginput-com-input" 
								value={this.state.enterValue}
								placeholder={this.props.placeholder} 
								style={inputStyle} 
								onChange={this.handleOnChangeInput.bind(this)} 
								onBlur={this.handleOnBlur.bind(this)}
								onFocus={this.handleOnFocus.bind(this)}
								onKeyDown={this.handleOnKeyDown.bind(this)} 
								onKeyUp={this.handleOnKeyUp.bind(this)} />
						</div>
					</div>
					<div className="clearfix"></div>
				</div>
				{isShowAutoCompletePanel && (
						<div className="mytaginput-com-wrapper-auto-complete">
							<div className="mytaginput-com-auto-complete-container">
								{this.state.tagsDatalist.map(function(val, idx){
									return (
										<div className={that.renderClassItemAutoComplete(idx)} key={idx} 
											onMouseDown={that.clickAddTag.bind(that, val)} 
											onMouseEnter={that.mouseEnterItem.bind(that, idx)} 
											onMouseLeave={that.mouseLeaveItem.bind(that)}>{val}</div>
									)
								})}
							</div>
						</div>
					)
				}
			</div>
		);
	}
}