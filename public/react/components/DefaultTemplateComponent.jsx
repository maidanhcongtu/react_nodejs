import React from 'react';

import HeaderComponent from "./HeaderComponent.jsx";
import FooterComponent from "./FooterComponent.jsx";

export default class DefaultTemplateComponent extends React.Component {
	
	render () {
		return (
			<div className="app-container">
				<div id="header-component">
					<HeaderComponent />
				</div>
				<div>
					{this.props.children}
				</div>
				<div id="footer-component">
					<FooterComponent />
				</div>
			</div>
		);
	}
}