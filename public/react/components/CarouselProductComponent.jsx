import React from 'react';
import {Link} from 'react-router';
import {ME} from '../helpers/me.js';


export default class CarouselComponent extends React.Component {

	constructor() {
	    super();
	    this.state = { showSubMenu : false, products: [1,2,3,4,5,6,7,8,9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,20,21,22,23], hideBackButton: false };
	    this.handleNextButton = this.handleNextButton.bind(this);
	    this.handleBackButton = this.handleBackButton.bind(this);
	}

	renderItemProducts() {
		return this.state.products.map(function(item, index){
			return <ItemProductComponent key={index}/>;
		});
	}

	componentDidMount() {
		//disable back btn
		this.showHideBackNextButton();
	}

	showHideBackNextButton(targetNextBack) {
		var contentObj = $(".carousel-product-container .content");
		if(targetNextBack !== undefined) {
			contentObj = $(targetNextBack).parents(".content");
		}
		var offsetLeftItemContainer = $(contentObj).find(".roll-item-wrapper").offset().left;
		var offsetRightItemContainer = offsetLeftItemContainer + $(contentObj).find(".roll-item-wrapper").width();
		var offsetFirstItem = $(contentObj).find(".item-container > .item:first-child").offset().left;
		var offsetLastItem = $(contentObj).find(".item-container > .item:last-child").offset().left;
		var nextButton = $(contentObj).find(".next-button");
		var backButton = $(contentObj).find(".back-button");
		
		//process back btn
		if(offsetFirstItem < offsetLeftItemContainer) {
			//show back button
			$(backButton).removeClass("hidden-back-btn");
		} else {
			//hide back button
			$(backButton).addClass("hidden-back-btn");
		}

		//process next btn
		if(offsetLastItem <= offsetRightItemContainer) {
			//hide next btn
			$(nextButton).addClass("hidden-next-btn");
		} else {
			//show next btn
			$(nextButton).removeClass("hidden-next-btn");
		}
	}

	handleNextButton(event) {
		var component = this;
		const eventTaget = event.target;

		var processing = $(eventTaget).attr("processing");
		if(processing !== "true") {
			//add attribute processing
			$(eventTaget).attr("processing","true");

			var itemContainer = $(eventTaget).parents(".content").find(".item-container");
			var beforeMarginLeft = $(itemContainer).css("left");
			var widthItemContainerWrapper = $(itemContainer).width();
			var nextLeft = parseInt(beforeMarginLeft) - parseInt(widthItemContainerWrapper);
			$(itemContainer).animate({"left": nextLeft + "px"}, 500, function(){
				//check show hidden
				component.showHideBackNextButton(eventTaget);
				//remove attribute processing
				$(eventTaget).removeAttr("processing");
			});
		}
	}

	handleBackButton(event) {
		var component = this;
		const eventTaget = event.target;

		var processing = $(eventTaget).attr("processing");
		if(processing !== "true") {
			//add attribute processing
			$(eventTaget).attr("processing","true");
			var itemContainer = $(eventTaget).parents(".content").find(".item-container");
			var beforeMarginLeft = $(itemContainer).css("left");
			var widthItemContainerWrapper = $(itemContainer).width();
			var backLeft = parseInt(beforeMarginLeft) + parseInt(widthItemContainerWrapper);
			$(itemContainer).animate({"left": backLeft + "px"}, 500, function(){
				//check show hidden
				component.showHideBackNextButton(eventTaget);

				//remove attribute processing
				$(eventTaget).removeAttr("processing");
			});
		}
	}

	render () {
		return (
			<div className="carousel-product-container">
				<div className="title-wrapper">
					<div className="line"></div>
					<div className="title">
						ko co cau tra loi
					</div>
				</div>
				<div className="content">
					<div className="back-button">
						<i className="fa fa-chevron-left back-icon" onClick={this.handleBackButton} aria-hidden="true"></i>
					</div>
					<div className="roll-item-wrapper">
						<div className="item-container">
							{this.renderItemProducts()}
						</div>
					</div>
					<div className="next-button">
						<i className="fa fa-chevron-right next-icon" onClick={this.handleNextButton} aria-hidden="true"></i>
					</div>
				</div>
			</div>
		);
	}
}

class ItemProductComponent extends React.Component {
	render() {
		return (
			<div className="item">
				<div className="img-wrapper">
					<Link to={ME.publicAPI + "/san-pham/key-san-pham-dung-cho-seo-1234"}>
						<img src="/public/react/prod/images/logo.jpg"/>
					</Link>
				</div>
				<div className="info-p-wrapper">
					<Link to={ME.publicAPI + "/san-pham/key-san-pham-dung-cho-seo-1234"} className="no-underline">
						<span className="p-name">cai gif cung va dieu cos thoi gian</span>
						<span className="p-price">Giá 500,00 đ</span>
					</Link>
				</div>
			</div>
		);
	}
}