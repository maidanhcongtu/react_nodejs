import React from 'react';
import Logo from ".././asserts/images/logo.jpg";

class HeaderComponentSubMenu extends React.Component {
	render() {
		return (
			<div className="sub-menu-c" onMouseEnter={this.props.handleMouseEnterSubMenu} onMouseLeave={this.props.handleMouseLeaveSubMenu}>
				sub me nu herer
			</div>
		);
	}
}

export default class HeaderComponent extends React.Component {
	constructor() {
	    super();
	    this.state = { showSubMenu : false };
	    this.handleMouseEnterMenu = this.handleMouseEnterMenu.bind(this)
	    this.handleMouseLeaveMenu = this.handleMouseLeaveMenu.bind(this)
	}

	//state {}

	handleMouseEnterMenu () {
		this.setState({showSubMenu: true});
	}

	handleMouseLeaveMenu () {
		this.setState({showSubMenu: false});
	}

	render () {
		return (
			<div className="header-container">
				<div className="h-c-t">
					<div className="header-content">
						<div className="h-c-left">
							<a href="">Cửa hàng</a>
							<a href="">Trợ giúp</a>
							<a href="">Hotline 098 62 33 169</a>
						</div>
						<div className="h-c-right">
							<a href="">THÔNG BÁO</a>
							<a href="">GIỎ HÀNG</a>
							<a href="">ĐĂNG KÝ</a>
							<a href="">ĐĂNG NHẬP</a>
						</div>
					</div>
				</div>

				<div className="h-c-b">
					<div className="header-content">
						<div className="h-logo-c">
							<img src={Logo} />
						</div>
						<div className="h-search-c">
							<div className="full-w">
								<div className="search-input-c">
									<input type="" className="fe-input" placeholder={"Tìm kiếm sản phẩm..."}/>
									<button className="fe-btn">
										<i className="fa fa-search" aria-hidden="true"></i>
									</button>
								</div>
								<div className="favorite-keys">
									<label className="">Có Thể Bạn Thích:</label>
									<a href="">ok man here</a>
									<a href="">ok man here</a>
									<a href="">ok man here</a>
									<a href="">ok man here</a>
									<a href="">ok man here</a>
								</div>
							</div>	
						</div>
					</div>
				</div>

				<div className="h-c-b-2">
					<div className="main-menu-c">
						<a href="" onMouseEnter={this.handleMouseEnterMenu} onMouseLeave={this.handleMouseLeaveMenu}>item menu</a>
						<a href="" onMouseEnter={this.handleMouseEnterMenu} onMouseLeave={this.handleMouseLeaveMenu}>item menu</a>
						<a href="" onMouseEnter={this.handleMouseEnterMenu} onMouseLeave={this.handleMouseLeaveMenu}>item menu</a>
						<a href="" onMouseEnter={this.handleMouseEnterMenu} onMouseLeave={this.handleMouseLeaveMenu}>item menu</a>
						<a href="" onMouseEnter={this.handleMouseEnterMenu} onMouseLeave={this.handleMouseLeaveMenu}>item menu</a>
					</div>
					{ this.state.showSubMenu ? <HeaderComponentSubMenu handleMouseEnterSubMenu={this.handleMouseEnterMenu} handleMouseLeaveSubMenu={this.handleMouseLeaveMenu}/> : null }
				</div>
			</div>
		);
	}
}