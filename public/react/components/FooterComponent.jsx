import React from 'react';

export default class FooterComponent extends React.Component {
	render() {
		return (
			<div className="footer-container">
				<div className="f-c-t">
					<div className="f-c-c">
						<div className="f-c-connect-us">
							<div className="title">KẾT NỐI VỚI CHÚNG TÔI</div>

							<div className="icon">
								<i className="fa fa-facebook" aria-hidden="true"></i>
								<i className="fa fa-youtube-play icon-detail" aria-hidden="true"></i>
							</div>

							<div className="contact-info">
								<div><strong>Hotline</strong> 098 62 33 169</div>
								<div><strong>Email</strong> ferino@gmail.com</div>
							</div>
						</div>

						<div className="f-c-receive-info">
							<div className="title">Cập nhật thông tin mới nhất</div>
							<div>Để nhận bản tin khuyến mãi, sản phẩm mới, tư vấn phong cách, quý khách vui lòng điền email vào bên dưới</div>
							<div className="frm-submit">
								<input type="text" className="fe-input" placeholder="email..."/> <button className="fe-btn fe-btn-primary">Gởi</button>
							</div>
						</div>
					</div>
				</div>

				<div className="f-c-menu">

				</div>

				<div className="f-c-b">
					<div className="f-c-c">
						<div><strong>Công ty TNHH Ferino</strong></div>
						<div>Địa chỉ: 39B - Trường Sơn - Tân Bình - HCM. Hotline: 098 62 33 169 - Email: ferino@gmail.com</div>
						<div>© 2015 - Bản quyền thuộc về Công ty TNHH Ferino</div>
					</div>
				</div>
			</div>
		);
	}
}

