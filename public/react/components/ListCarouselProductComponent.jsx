import React from 'react';
import CarouselProductComponent from "./CarouselProductComponent.jsx";

export default class ListCarouselProductComponent extends React.Component {

	constructor() {
	    super();
	    this.state = { carouselProducts: [1,2, 3, 4, 5] };
	}

	renderListCarouselProduct() {
		return this.state.carouselProducts.map(function(item, index){
			return (<CarouselProductComponent key={index} />);
		});
	}

	render () {
		return (
			<div className="list-carousel-product-container">
				{this.renderListCarouselProduct()}
			</div>
		);
	}
}