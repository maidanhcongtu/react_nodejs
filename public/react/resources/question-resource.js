import {ME} from '../helpers/me.js';
import axios from 'axios';

const questionAPi = "/api/admin/questions";

class QuestionResource {
  	
  	save(question) {
      return axios.post(questionAPi, question);
  	}

    update(question) {
      return axios.put(questionAPi, question);
    }

    getAll() {
      return axios.get(questionAPi);
    }

    deleteById(questionID) {
      return axios.delete(questionAPi + "/" + questionID);
    }
    /*fromDate: milisecond, toDate: millisecond*/
    getByTagsAndTypeAndYearMonthDay(tags, type, fromDate, toDate) {
      return axios.get(questionAPi + "/filter", {
        params: {
          tags: tags,
          type: type,
          fromDate: fromDate,
          toDate: toDate
        }
      });
    }
}

const questionResource = new QuestionResource();
export default questionResource;
