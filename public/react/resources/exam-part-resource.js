import {ME} from '../helpers/me.js';
import axios from 'axios';

const examPartAPi = "/api/admin/exam-parts";

class ExamPartResource {
  	
  	save(examPart) {
      return axios.post(examPartAPi, examPart);
  	}

    update(examPart) {
      return axios.put(examPartAPi, examPart);
    }

    getAll() {
      return axios.get(examPartAPi);
    }

    deleteById(examPartId) {
      return axios.delete(examPartAPi + "/" + examPartId);
    }
    /*fromDate: milisecond, toDate: millisecond*/
    getByTagsAndTypeAndYearMonthDay(tags, type, fromDate, toDate) {
      return axios.get(examPartAPi + "/filter", {
        params: {
          tags: tags,
          type: type,
          fromDate: fromDate,
          toDate: toDate
        }
      });
    }
}

const examPartResource = new ExamPartResource();
export default examPartResource;
