import {ME} from '../helpers/me.js';
import axios from 'axios';

const TagAPI = "/api/admin/tags";

class TagResource {
    /*param type: question, exam-part, exam*/
    getByType(type) {
      return axios.get(TagAPI + "/" + type);
    }
}

const tagResource = new TagResource();
export default tagResource;
